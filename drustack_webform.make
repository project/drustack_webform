api = 2
core = 7.x

; Modules
projects[webform][download][tag] = 7.x-4.5
projects[webform][download][type] = git
projects[webform][subdir] = contrib
projects[webform_protected_downloads][download][tag] = 7.x-1.0
projects[webform_protected_downloads][download][type] = git
projects[webform_protected_downloads][subdir] = contrib
